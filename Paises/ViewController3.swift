//
//  ViewController3.swift
//  Paises
//
//  Created by carlos pumayalla on 9/28/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {
    
    var img = UIImage()

    @IBOutlet weak var imagen: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagen.image = img
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
