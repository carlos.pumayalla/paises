//
//  ViewController.swift
//  Paises
//
//  Created by carlos pumayalla on 9/28/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var Tabla: UITableView!
    var contenido = ["Perù", "Mexico", "Brasil"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Tabla.dataSource = self
        Tabla.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contenido.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
        celda.textLabel?.text = contenido[indexPath.row]
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let paisSelecionado = indexPath.row
        self.performSegue(withIdentifier: "destinosSegue", sender: paisSelecionado)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "destinosSegue"{
            let idpais = sender as! Int
            let pantalla2:ViewController2 = segue.destination as! ViewController2
            pantalla2.idPais = idpais
        }
    }
    
}



