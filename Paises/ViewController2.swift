//
//  ViewController2.swift
//  Paises
//
//  Created by carlos pumayalla on 9/28/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var destinos = [
    ["Machu Picchu", "Mancora", "Lineas de Nazca"],
    ["Piramide Maya", "Can Cun", "Acapulco"],
    ["Rio de Janeiro", "Cristo Redentor", "Fabelas"]    ]
    var idPais : Int!

    @IBOutlet var Tabla2: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Tabla2.dataSource = self
        Tabla2.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        destinos[idPais].count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
        celda.textLabel?.text = destinos[idPais][indexPath.row] 
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destinoSelecionado = indexPath.row
        self.performSegue(withIdentifier: "imageSegue", sender: destinoSelecionado)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageSegue"{
            let idDestino = sender as! Int
            let pantalla3:ViewController3 = segue.destination as! ViewController3
            pantalla3.img = UIImage(named: destinos[idPais][idDestino])!
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
